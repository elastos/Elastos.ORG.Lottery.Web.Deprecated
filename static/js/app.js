(function($, owner) {
	owner.getAddress = function(){
		return "http://localhost:8081/";
	};
	owner.getUrlParam=function(name){
		  var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i");   
	      var r = window.location.search.substr(1).match(reg);   
	      if (r != null) return decodeURI(r[2]); return null;   
	};
	owner.fillDetailForm=function(json_data){
		for(var key in json_data){
　　　　   //console.log(key+':'+json_data[key]);
           if(key=="status"){
			   if(json_data[key]=="0"){
				   $("input[name='status']:first").attr('checked', 'checked');
			   }else{
				   $("input[name='status']:last").attr('checked', 'checked');
			   }
		   }else if(key=="pic"){
			   $("#"+key).attr("src",this.getAddress()+json_data[key]);
		   }else {
			   $("#"+key).val(json_data[key]);
		   }
           
　　    }
	};
	owner.checkHasChange=function(old_data,new_data){
		for(var key in new_data){
			if(old_data[key]!=new_data[key]){
				return true;
			}
		}
		return false;
	};
	owner.time_range = function(beginTime, endTime, nowTime) { 
		var strb = beginTime.split(":");
		if(strb.length != 2) {
			return false;
		}
		var stre = endTime.split(":");
		if(stre.length != 2) {
			return false;
		}

		var strn = nowTime.split(":");
		if(stre.length != 2) {
			return false;
		}
		var b = new Date();
		var e = new Date();
		var n = new Date();

		b.setHours(strb[0]);
		b.setMinutes(strb[1]);
		e.setHours(stre[0]);
		e.setMinutes(stre[1]);
		n.setHours(strn[0]);
		n.setMinutes(strn[1]);

		if(n.getTime() - b.getTime() > 0 && n.getTime() - e.getTime() < 0) {
			return true;
		} else {
			console.log("褰撳墠鏃堕棿鏄細" + n.getHours() + ":" + n.getMinutes() + "锛屼笉鍦ㄨ鏃堕棿鑼冨洿鍐咃紒");
			return false;
		}	
	};
	owner.calculateDistance= function getGreatCircleDistance(lat1,lng1,lat2,lng2){
	    var EARTH_RADIUS = 6378137.0;   
	    var PI = Math.PI;
		var radLat1 = lat1*PI/180.0;
        var radLat2 = lat2*PI/180.0;
        var a = radLat1 - radLat2;
        var b = lng1*PI/180.0 - lng2*PI/180.0;
        var s = 2*Math.asin(Math.sqrt(Math.pow(Math.sin(a/2),2) + Math.cos(radLat1)*Math.cos(radLat2)*Math.pow(Math.sin(b/2),2)));
        s = s*EARTH_RADIUS;
        s = Math.round(s*10000)/10000.0;
        if(s>1000){
        	s=(s/1000).toFixed(2)+"里";
        }else{
        	s=s+"米";
        }
        return s;
    };
	owner.getDetailNowDate=function getNowFormatDate() {
	    var date = new Date();
	    var seperator1 = "-";
	    var seperator2 = ":";
	    var month = date.getMonth() + 1;
	    var strDate = date.getDate();
	    if (month >= 1 && month <= 9) {
	        month = "0" + month;
	    }
	    if (strDate >= 0 && strDate <= 9) {
	        strDate = "0" + strDate;
	    }
	    var currentdate = date.getFullYear() + seperator1 + month + seperator1 + strDate+" "+date.getHours()+seperator2+date.getMinutes();
	    return currentdate;
	};
    owner.isNull=function(par){
    	if(par==null||par==undefined||par==""){
    		return true;
    	}
    	return false;
    };
	owner.checkPhone=function(phone){
		var myreg = /^(((13[0-9]{1})|(15[0-9]{1})|(18[0-9]{1}))+\d{8})$/; 
		if(!myreg.test(phone)) 
		{ 
		    mui.toast('请输入有效的手机号码！'); 
		    return false; 
		} 
		return true;
	};
	owner.ajaxRequest=function(method,param,successCallBack,failCallBack){
		var url=" http://localhost:8081/"+method;
		$.ajax({   
		      url: url,   
		      data:param,
		      dataType: 'json',   
		      type: 'post',   
		      scriptCharset: 'utf-8',
		      success:function(data){
		    	  successCallBack(data) 
		      },
		      error:function(er){
				  console.log(er);
		    	  failCallBack(er);
		      }
	    });  
	};
	owner.ajaxGetRequest=function(method,param,successCallBack,failCallBack){
		var url=" http://localhost:8081/"+method;
		$.ajax({   
		      url: url,   
		      data:param,
		      dataType: 'json',   
		      type: 'get',   
		      scriptCharset: 'utf-8',
		      success:function(data){
		    	  successCallBack(data) 
		      },
		      error:function(er){
				  console.log(er);
		    	  failCallBack(er);
		      }
	    });  
	};
	owner.ajaxRequestAsync=function(method,param,successCallBack,failCallBack){
		var url=" http://localhost:8081/"+method;
		$.ajax({   
		      url: url,   
		      data:param,
		      async:false,
		      dataType: 'json',   
		      type: 'post',   
		      scriptCharset: 'utf-8',
		      success:function(data){
		    	  successCallBack(data) 
		      },
		      error:function(er){
		    	  failCallBack(er);
		      }
	    });  
	};
	owner.getNowDate=function getNowFormatDate() {
	    var date = new Date();
	    var seperator1 = "-";
	    var seperator2 = ":";
	    var month = date.getMonth() + 1;
	    var strDate = date.getDate();
	    if (month >= 1 && month <= 9) {
	        month = "0" + month;
	    }
	    if (strDate >= 0 && strDate <= 9) {
	        strDate = "0" + strDate;
	    }
	    var currentdate = date.getFullYear() + seperator1 + month + seperator1 + strDate;
	    return currentdate;
	};
	owner.getNowDetailDate=function getNowFormatDate() {
	     var date = new Date();
	    var seperator1 = "-";
	    var seperator2 = ":";
	    var month = date.getMonth() + 1;
	    var strDate = date.getDate();
	    if (month >= 1 && month <= 9) {
	        month = "0" + month;
	    }
	    if (strDate >= 0 && strDate <= 9) {
	        strDate = "0" + strDate;
	    }
	    var currentdate = date.getFullYear() + seperator1 + month + seperator1 + strDate+" "+date.getHours()+seperator2+date.getMinutes();
	    return currentdate;
	};
	owner.compardate=function (d1,d2)
	{
	  return ((new Date(d1.replace(/-/g,"\/"))) > (new Date(d2.replace(/-/g,"\/"))));
	};
	owner.showDialog=function(html){
		var bodyele=document.getElementsByTagName("body")[0] ;
		var span=document.createElement("span");
		span.innerHTML=html;
		span.setAttribute("style","position: fixed;left: 40%; background-color:white;border-radius: 10px;height: 30px;line-height:30px ;color:black; width: 8.26666666667rem;text-align: center;bottom: 40px;font-size: 0.65rem; margin-left:-2.13333333333rem;");
		bodyele.appendChild(span);
		setTimeout(function(){
			bodyele.removeChild(span);
		},1000);
	}
}(jQuery, window.app = {}));
