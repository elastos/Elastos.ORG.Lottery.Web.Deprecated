package main

import (
	_ "Elastos.ORG.Lottery.Web/routers"
	"container/list"
	"github.com/astaxie/beego"
	"strconv"
	"strings"
)

func main() {
	beego.SetStaticPath("/static", "static")
	beego.Run()
}

func init() {
	beego.AddFuncMap("trimLast", func(in string) (out string) {
		return in[0 : len(in)-1]
	})
	beego.AddFuncMap("lastStr", func(in string) (out string) {
		return in[len(in)-1:]
	})
	beego.AddFuncMap("upper", func(in string) (out string) {
		return strings.ToUpper(in)
	})
	beego.AddFuncMap("strToFloat", func(in string) (out float64) {
		out, err := strconv.ParseFloat(in, 64)
		if err != nil {
			beego.Error(err)
		}
		return
	})
	beego.AddFuncMap("listLen", func(in list.List) (out int) {
		return in.Len()
	})

}
