<!DOCTYPE html>
<html>

	<head>
		<meta charset="utf-8">
		<title>Elastos</title>
		<meta name="viewport" content="width=device-width, initial-scale=1,maximum-scale=1, user-scalable=no">
		<meta name="apple-mobile-web-app-capable" content="yes">
		<meta name="apple-mobile-web-app-status-bar-style" content="black">
		<link rel="stylesheet" type="text/css" href="/static/css/base.css" />
		<link rel="stylesheet" type="text/css" href="/static/css/mui.min.css" />
		<link rel="stylesheet" type="text/css" href="/static/css/index.css" />
		<script src="/static/js/mui.min.js" type="text/javascript" charset="utf-8"></script>
        <meta http-equiv="refresh" content="15" ></head>
	</head>
    <style>
        .pointer {cursor: pointer;}
    </style>
	<body style="background: url(/static/images/Blocks.jpg);" id="body_ele">
		<!--<img src="http://jcz4pf.natappfree.cc/ceshi/images/Blocks.jpg" id="blockImg" />-->
		<div class="content_div">
			<img src="/static/images/Lottery.png" class="lootery" />
			<p class="desc">若选中的字符与Elastos Blockchain生成的区块Hash值末位值相同，即为押中获奖。
				<a href="/lotteryall?openid={{.openid}}">查看历史</a>
			</p>
			<p class="desceng">You will be reward if your input is same to the last letter of Hash value generated by the Elastos Blockchain.
				<a href="/lotteryall?openid={{.openid}}">&nbsp;&nbsp;View history</a>
			</p>
			<div class="btn_p pointer">
				<p>上期区块 #{{.blockReward.LastBlock}} 末位字符： {{.blockReward.LastBlockHash | lastStr | upper}}</p>
				<p>Hash 值:{{.blockReward.LastBlockHash | trimLast }}<span>{{.blockReward.LastBlockHash | lastStr | upper}}</span></p>
			</div>
			<p class="now_block">本期区块#{{.blockReward.CurrBlock}}</p>
			<div class="number_div">
				<div data-number="0">0</div>
				<div data-number="1">1</div>
				<div data-number="2">2</div>
				<div data-number="3">3</div>
				<div data-number="4">4</div>
				<div data-number="5">5</div>
				<div data-number="6">6</div>
				<div data-number="7">7</div>
				<div data-number="8">8</div>
				<div data-number="9">9</div>
				<div data-number="a">a</div>
				<div data-number="b">b</div>
				<div data-number="c">c</div>
				<div data-number="d">d</div>
				<div data-number="e">e</div>
				<div data-number="f">f</div>
			</div>
			<div class="sumit_btn pointer" onclick="submitToElastos()">
				<p>提交至Elastos区块链</p>
				<p>Update to Elastos Blockchain</p>
			</div>
			<br/>
		</div>
	</body>
	<script src="/static/js/jquery-3.2.1.min.js"></script>
	<script src="/static/js/app.js"></script>
	<script>
		var choosed_numbers = [];
		$(".number_div div").click(function(event) {
			var number = $(event.target).attr("data-number");
			choosed_numbers=[];
			choosed_numbers.push(number);
			var my = $(this).index();
			$(this).addClass('number_div_fill').siblings().removeClass('number_div_fill');
			console.log(choosed_numbers);
		})
		function submitToElastos() {
			// app.showDialog("选择的数字=" + choosed_numbers.toString());
			$(".number_div div").each(function() {
				$(this).removeClass("number_div_fill");
			})
			// choosed_numbers = [];
            if (choosed_numbers.length == 0 ){
			    alert("Please Choose First")
                return
            }
            retObj = $.ajax({url:"/isGuessed?openid="+{{.openid}},async:false});
            console.log(retObj)
            if(retObj.status == 200 && JSON.parse(retObj.responseText).Error == 0){
                var redirectUrl = "{{.net}}/#?is_pay=true&account={{.sendEla}}&address={{.walletAddr}}&memo=chinajoylottery@"+choosed_numbers.toString()+"@{{.blockReward.CurrBlock}}@{{.openid}}&callback_url={{.CallbackUrl}}{{.openid}}"+choosed_numbers.toString()+"&return_url={{.returlUrl}}{{.openid}}"
                window.location.href=redirectUrl
            }else if (retObj.status == 200 && JSON.parse(retObj.responseText).Error == -2){
                alert("already guessed this block")
				return
			}else if (retObj.status == 200 && JSON.parse(retObj.responseText).Error == -3){
                alert("您已经中过奖了，不能再猜了")
                return
            }

		}
        $(".btn_p").click(function(){
            window.location.href = "/lotterydetail/{{.blockReward.LastBlock}}"
        })
		function deviceMotionHandler(eventData) {
			var acceleration = eventData.accelerationIncludingGravity;
			var facingUp = -1;
			if(acceleration.z > 0) {
				facingUp = +1;
			}
			var tiltLR = Math.round(((acceleration.x) / 9.81) * -90);
			var tiltFB = Math.round(((acceleration.y + 9.81) / 9.81) * 90 * facingUp);
			var rotation = "rotate(" + tiltLR + "deg) rotate3d(1,0,0, " + (tiltFB) + "deg)";
			console.log(rotation);
			document.getElementById("blockImg").style.webkitTransform = rotation;
		}
		var body_ele = document.getElementById("body_ele");
		window.addEventListener("deviceorientation", function(event) {
			console.log(event);
			var alpha = event.alpha;
			var beta = event.beta;
			var gamma = event.gamma;
			var z_pos=beta/2;
			var left_pos=alpha/8;//左倾斜
			var right_pos=gamma/2;//右边倾斜
			$(body_ele).attr("style", "background: url(/static/images/Blocks.jpg); background-position:" + right_pos + "% "+z_pos+"%");
			//mui.toast(alpha + "-" + beta + "-" + gamma);
		}, true);
	</script>
	<script>
		var index = 0;
		var body_ele = document.getElementById("body_ele");
		setTimeout(function(){
			console.log(1111);
			index=index+10;
			//$(body_ele).attr("style", "background: url(elas/images/Blocks.jpg); background-position:" + index + "%");
		}, 1000);
	</script>

</html>