<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>Ardbord</title>
    <link rel="stylesheet" type="text/css" href="/static/css/base.css" />
    <link rel="stylesheet" type="text/css" href="/static/css/artboard.css" />
    <meta http-equiv="refresh" content="15" />
</head>

<body>
<div class="bg_fixed_div artboard-bg">
    <!--数据列表-->
    <div class="list_content">
        <div class="content_header">
            <span>{{.blockinfo.timestamp}}</span>
            <p>Block #{{.blockinfo.height}}</p>
            <p class="font-eng">Hash:{{.blockinfo.blockhash}}</p>
        </div>
        <div class="list">
            <div class="list_header">
                <p>类型</p>
                <p>用户</p>
                <p>内容</p>
                <p>Hash</p>
            </div>
            <div class="list_data" id="datas_list">
                {{range $i , $v := .txinfo }}
                <div class="list_cell">
                    <p>{{$v.type}}</p>
                    <p>{{$v.user}}</p>
                    <p>{{$v.content}}</p>
                    <p>{{$v.txid}}</p>
                </div>
                {{end}}
            </div>
        </div>
    </div>
    <!--遮阳伞 棕建树-->
    <div class="palm_div">
        <img src="/static/images/palm tree left.png" class="palm_tree palm_clock_left" />
        <img src="/static/images/umbrella.png" class="umbrella palm_clock_umbrella" />
        <img src="/static/images/palm tree right.png" class="palm_tree palm_tree_right palm_clock_right" />
    </div>
    <img src="/static/images/beach.png" class="beach" />
</div>
</body>
<script src="/static/js/jquery-3.2.1.min.js"></script>
<script>
    var i=0;
    var datas_list=document.getElementById("datas_list");
    var animation = {};
    animation.duration = 300;
    animation.easing ="swing";
    // //模拟加载数据
    // function addCell(){
    //     i++;
    //     var cells=document.getElementsByClassName("list_cell");
    //     $(".list_cell").each(function(){
    //         $(this).attr("style","");
    //     })
    //     var list_cell=document.createElement("div");
    //     list_cell.setAttribute("style","height: 0px;opacity: 0;")
    //     list_cell.className='list_cell';
    //     var cell_str='<p>游戏记录'+i+'/成绩</p><p>我是第一个用户</p><p>第五关挑战失败</p><p>1234564564654564654564564564 | 454565645621q234456</p>';
    //     list_cell.innerHTML=cell_str;
    //     //datas_list.insertBefore(list_cell,cells[0]);
    //     datas_list.insertBefore(list_cell,cells[0]);
    //     $(list_cell).animate({height:'50px',opacity:'1'},"slow");
    //     $(cells[1]).animate({marginTop:'50px'},"slow");
    //     cells=document.getElementsByClassName("list_cell");
    // }
</script>
</html>