<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>Main</title>
    <meta name="viewport" content="width=device-width, initial-scale=1,maximum-scale=1, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <link rel="stylesheet" type="text/css" href="/static/css/base.css" />
    <script src="/static/js/mui.min.js"></script>
    <meta http-equiv="refresh" content="15" ></head>
</head>
<style>
    p{
        text-align: center;
        margin: 0;
    }
    .btn{
        margin-top: 20px;
    }
    .get_text{
        margin-top: 4px;
    }
    .pointer {cursor: pointer;}

</style>

<body background="/static/images/Star.png" id="body_ele">
<div class="layer layer_two"
     style="background-image: url(/static/images/Net.png); background-position: 50% 80%;"
     id="Net"></div>
<div class="layer layer_four" id="content">
    <img src="/static/images/ELA.png" class="Ela_img" />
    {{if gt (.tickets | len) 0}}
        {{range $i , $v := .tickets}}
            <p class="color_get_yellow get_text">{{if eq $v.movie_ticket "Out Of Tickets, Sorry"}}{{$v.movie_ticket}}{{end}}{{if ne $v.movie_ticket "Out Of Tickets, Sorry"}}电影票码 :{{$v.movie_ticket}},{{$v.movie_ticket1}}{{end}} </p>
        {{end}}
    {{end}}
<!--
    <div class="border_white_2px_nocorner btn pointer" id="register" style="margin-top: 5px;display: none">
        <p>进入 Elastos 钱包</p>
        <p>Enter Elastos Wallet</p>
    </div>
-->
    <div class="border_white_2px_nocorner btn pointer" id="lottery">
        <p>Elastos 欢乐竞猜</p>
        <p>Elastos Lottery</p>
    </div>
    <!--
    <div class="border_white_2px_nocorner btn pointer" id="game" style="display: none">
        <p>Elastos 游戏中心</p>
        <p>Elastos Game Center</p>
    </div>
    -->
    <p class="help pointer" id="edit">修改钱包 Edit Wallet</p>
</div>
<div class="layer layer_three">
    <img src="/static/images/Whale.png" class="whale_img" />
</div>
</body>
<script src="/static/js/jquery-3.2.1.min.js"></script>
<script>
    $("#register").click(function (obj) {
        window.location.href = "{{.registerWalletUrl}}"
    })

    $("#lottery").click(function (obj) {
        window.location.href = "{{.lotteryAddr}}?openid={{.openid}}"
    })

    $("#game").click(function (obj) {
        window.location.href = "{{.gameAddr}}?openid={{.openid}}"
    })

    $("#edit").click(function () {
        window.location.href = "/index/?openid={{.openid}}&isModify=1"
    })
</script>
<!--重力感应相关js-->
<script>
    var body_ele = document.getElementById("body_ele");
    var last_update=0;//定义一个变量记录上一次移动
    var x=y=z=last_x=last_y=last_z=0;//定义x、y、z记录三个轴的数据以及上一次触发的时间
    var SHAKE_THRESHOLD=50;
    var now_left=0;
    window.addEventListener("devicemotion",function(event){
        console.log(event);
        var acceleration = event.accelerationIncludingGravity;
        var curTime=new Date().getTime();//获取当前时间戳
        var diffTime=curTime-last_update;
        if(diffTime>100){
            last_update=curTime;//记录上一次摇动的时间
            x=acceleration.x;//获取加速度X方向
            y=acceleration.y;//获取加速度Y方向
            z=acceleration.z;//获取加速度垂直方向
            var speed=Math.abs(x+y+z-last_x-last_y-last_z)/diffTime*10000;//计算阈值
            if(speed>SHAKE_THRESHOLD){
                // mui.toast(speed);
                window.addEventListener("deviceorientation", function(event) {
                    var alpha = event.alpha;
                    var beta = event.beta;
                    var gamma = event.gamma;
                    var z_pos = beta / 2;
                    var left_pos = alpha / 8;
                    var right_pos = gamma / 2;
                    var z_pos_whale=alpha;
                    //mui.toast(Math.floor(alpha * 100) / 100+"/"+Math.floor(beta * 100) / 100+"/"+Math.floor(gamma * 100) / 100);
                    $(body_ele).attr(
                            "style",
                            "background: url(/static/images/Star.png); background-position:" +
                            right_pos + "% " + z_pos + "%");
                    if(Math.abs(gamma)>2||Math.abs(beta)){
                        if(gamma<0){
                            if((z_pos+40)>100){return;}
                            $("#Net").attr(
                                    "style",
                                    "background: url(/static/images/Net.png); background-position:" +
                                    (50-right_pos) + "% " + (z_pos+40) + "%");
                        }else{
                            if((z_pos+40)>100){return;}
                            $("#Net").attr(
                                    "style",
                                    "background: url(/static/images/Net.png); background-position:" +
                                    (right_pos+50) + "% " + (z_pos+40) + "%");
                        }
                        if(right_pos>0){
                            now_left=right_pos;
                        }else{
                            now_left=z_pos;
                        }

                        // if(right_pos<0){
                        $("#whale_img").attr(
                                "style",
                                "bottom:" +
                                (z_pos/5) + "%;left:" + (right_pos) + "%");
                        // }

                    }
                }, true);
            }
            //记录上一次加速度
            last_x=x;
            last_y=y;
            last_z=z;
        }

    });
</script>
</html>