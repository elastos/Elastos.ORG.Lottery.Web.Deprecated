<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>Welcome</title>
    <meta name="viewport" content="width=device-width, initial-scale=1,maximum-scale=1, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <link rel="stylesheet" type="text/css" href="/static/css/base.css" />
</head>
<style>
    .pointer {cursor: pointer;}
</style>
<body background="/static/images/Star.png" id="body_ele" style="ba">
<div class="layer layer_two"
     style="background-image: url(/static/images/Net.png); background-position: 50% 80%;"
     id="Net"></div>
<div class="layer layer_four" id="content">
    <img src="/static/images/ELA.png" class="Ela_img" id="ela_img" />
    <div class="border_white_2px_nocorner btn pointer" id="register">
        <p>注册 Elastos 钱包</p>
        <p>Register Elastos Wallet</p>
    </div>
    <div class="border_white_2px_nocorner btn" contenteditable="true" id="wallet_address">
        <p>输入钱包地址</p>
        <p>Enter Wallet Address</p>
    </div>
    <img src="/static/images/Arrow.png" class="arrow_img pointer" />
</div>
<div class="layer layer_three">
    <img src="/static/images/Whale.png" class="whale_img" id="whale_img" />
</div>
<div class="back-top" style="display: none;"></div>
<div class="layer layer_five" style="display: none;">
    <img src="/static/images/ELA.png" class="Ela_img" />
    <div class="cache_content">
        <p class="letter_space_1px">您将收到一笔来自亦来云的奖励金，可用于 ChinaJoy 亦来云展位的竞猜、游戏等活动。</p >
        <p>You will receive a transfer from Elastos, that can be used in Elastos’ Pavilion at ChinaJoy.</p >
        <p class="letter_space_1px" style="font-size:11px">*区块链交易分布于各广泛节点，具有匿名化，去中心化等特点，有一定时间延迟。请 2 分钟后刷新界面。</p >
        <p style="font-size:11px">*Blockchain technology is decentralized and anonymous, the transfer is distributed in active blocks worldwide,  so it needs approximate 2 minutes to deposit to your wallet address. Please reload this page later.</p >
    </div>
    <img src="/static/images/Wave.gif" class="wave_img" />
</div>
</body>
<script src="/static/js/jquery-3.2.1.min.js"></script>
<script src="/static/js/mui.min.js" type="text/javascript" charset="utf-8"></script>
<script>
    var text_html = "";
    var input_html = '<input type="text" autofocus="autofocus" id="wallet_address_content" value="{{.addr}}"/>';
    var wallet_address = document.getElementById("wallet_address");
    wallet_address.addEventListener("focus", function(event) {
        text_html = wallet_address.innerHTML;
        wallet_address.innerHTML = input_html;
        $(".back-top").show();
        $("#register").addClass("disable_btn")
        $("#content").attr("style", "z-index: 15");
        $(".arrow_img").attr("style", "display: block;")
    });
    // $(".layer_four").click(function(){
    //     wallet_address.innerHTML = text_html;
    //     $(".arrow_img").attr("style", "display: none;")
    //     $(".back-top").hide();
    //     $("#register").removeClass("disable_btn")
    // })
    $(".arrow_img").click(function(event) {
        var addess_content = $("#wallet_address_content").val();
        if(addess_content == null || addess_content == undefined || addess_content == "" || addess_content.length != 34 || !addess_content.startsWith("E")) {
            //提示没有输入钱包地址
            alert("Please Fill in a valid ELA address");
            return
        }
        retObj = $.ajax({url:"/submitAddr/"+addess_content+"?vldCode="+{{.vldCode}}+"&openid="+{{.openid}},async:false});
        console.log(retObj)
        if(retObj.status == 200){
            window.location.href = "/home/"+{{.openid}}
        }else{
            alert(JSON.parse(retObj.responseText).Desc);
        }
        /*
        if(retObj.status == 200 && JSON.parse(retObj.responseText).Error == 0){
            $(this).attr("src", "/static/images/Wave.gif")
            $(".layer_five").show();
            setTimeout(callback,15000)
        }else if(retObj.status == 200 && JSON.parse(retObj.responseText).Error == 1){
            window.location.href = "/home/"+{{.openid}}
        }else{
            alert(JSON.parse(retObj.responseText).Desc);
        }*/
    })

    function callback(){
        retObj = $.ajax({url:"/registerUser/"+{{.openid}},async:false});
        console.log(retObj,JSON.parse(retObj.responseText))
        if(retObj.status == 200 && JSON.parse(retObj.responseText).Error == 0){
            location.href = "/home/"+{{.openid}}
        }
        setTimeout(callback,15000)
    }

    if ({{.registed}} === 1 ){
        // console.log("start callback")
        // $(this).attr("src", "/static/images/Wave.gif")
        // $(".layer_five").show();
        // setTimeout(callback,15000)
        location.href = "/home/"+{{.openid}}
    }

    $("#register").click(function (obj) {
        window.location.href = {{.registerWalletUrl}}
    })
</script>
<!--重力感应相关js-->
<script>
    var body_ele = document.getElementById("body_ele");
    var last_update=0;//定义一个变量记录上一次移动
    var x=y=z=last_x=last_y=last_z=0;//定义x、y、z记录三个轴的数据以及上一次触发的时间
    var SHAKE_THRESHOLD=50;
    var now_left=0;
    window.addEventListener("devicemotion",function(event){
        console.log(event);
        var acceleration = event.accelerationIncludingGravity;
        var curTime=new Date().getTime();//获取当前时间戳
        var diffTime=curTime-last_update;
        if(diffTime>100){
            last_update=curTime;//记录上一次摇动的时间
            x=acceleration.x;//获取加速度X方向
            y=acceleration.y;//获取加速度Y方向
            z=acceleration.z;//获取加速度垂直方向
            var speed=Math.abs(x+y+z-last_x-last_y-last_z)/diffTime*10000;//计算阈值
            if(speed>SHAKE_THRESHOLD){
                // mui.toast(speed);
                window.addEventListener("deviceorientation", function(event) {
                    var alpha = event.alpha;
                    var beta = event.beta;
                    var gamma = event.gamma;
                    var z_pos = beta / 2;
                    var left_pos = alpha / 8;
                    var right_pos = gamma / 2;
                    var z_pos_whale=alpha;
                    //mui.toast(Math.floor(alpha * 100) / 100+"/"+Math.floor(beta * 100) / 100+"/"+Math.floor(gamma * 100) / 100);
                    $(body_ele).attr(
                            "style",
                            "background: url(/static/images/Star.png); background-position:" +
                            right_pos + "% " + z_pos + "%");
                    if(Math.abs(gamma)>2||Math.abs(beta)){
                        if(gamma<0){
                            if((z_pos+40)>100){return;}
                            $("#Net").attr(
                                    "style",
                                    "background: url(/static/images/Net.png); background-position:" +
                                    (50-right_pos) + "% " + (z_pos+40) + "%");
                        }else{
                            if((z_pos+40)>100){return;}
                            $("#Net").attr(
                                    "style",
                                    "background: url(/static/images/Net.png); background-position:" +
                                    (right_pos+50) + "% " + (z_pos+40) + "%");
                        }
                        if(right_pos>0){
                            now_left=right_pos;
                        }else{
                            now_left=z_pos;
                        }

                        // if(right_pos<0){
                        $("#whale_img").attr(
                                "style",
                                "bottom:" +
                                (z_pos/5) + "%;left:" + (right_pos) + "%");
                        // }

                    }
                }, true);
            }
            //记录上一次加速度
            last_x=x;
            last_y=y;
            last_z=z;
        }
    });
</script>

</html>