<!DOCTYPE html>
<html>

<head>
<meta charset="utf-8">
<title>Elastos</title>
<meta name="viewport"
	content="width=device-width, initial-scale=1,maximum-scale=1, user-scalable=no">
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black">
<link rel="stylesheet" href="/static/css/mui.min.css">
<link rel="stylesheet" type="text/css" href="/static/css/base.css" />
<script src="/static/js/mui.min.js"></script>
</head>
<style>
    .detail_main_div {
        position: absolute;
        top: 10px;
        left: 0px;
        right: 0px;
        bottom: 30px;
        padding: 0px 10px;
    }

    .detail_main_div .title {
        margin-top: 18px;
        margin-bottom: 8px;
    }

    .detail_div_content {
        border: 2px solid white;
        border-radius: 10px;
        padding: 8px 12px;
        height: calc(100% - 69px);
        overflow-y: scroll;
    }

    .detail_div_content p {
        margin: 0px;
        font-size: 14px;
    }

    .detail_div_content p:first-child {
        margin-bottom: 5px;
    }

    .detail_div_content p:last-child span {
        padding-left: 5px;
        margin-left: -3px;
    }

    .content_fixed {
        position: fixed;
        z-index: 10000;
        background: black;
        left: 22px;
        right: 22px;
        top: 60px;
        padding-top: 8px;
    }

    .participants {
        margin-top: 100px;
    }

    .count {
        margin-left: 10px !important;
    }

    .participant {
        margin-top: 9px;
        font-size: 12px !important;
        position: relative;
    }

    .participant:last-child {
        margin-bottom: 24px;
    }

    .username {
        margin-left: 10px !important;
        padding: 0px !important;
    }

    .participant span:last-child {
        position: absolute;
        right: 12px;
    }
</style>

<body style="background: url(/static/images/Blocks.jpg);" id="body_ele">
	<div class="detail_main_div">
    {{if gt (.details | len) 0}}
		<p class="title">{{.blockinfo.update_time}}</p>
		<div class="detail_div_content">
			<div class="content_fixed">
				<p>
					区块 #{{.blockinfo.height}} 末位字符：<span>{{.blockinfo.blockhash | lastStr}}</span>
				</p>
				<p>Hash值：{{.blockinfo.blockhash}}</p>
                <p>
                    参与 participants:<span class="count">({{.details | len}})</span>
                </p>
			</div>
			<div class="participants">
            {{range $i, $v := .details}}
                <p class="participant {{if eq $v.status "1"}} color_get_yellow{{end}}">
                {{$v.create_time}}<span class="username">{{$v.nickname}}</span><span>{{$v.hexChar}}</span>
				</p>
			{{end}}
			</div>
		</div>
    {{end}}
    {{if eq (.details | len) 0}}
        <p class="title">当前区块没有用户参与竞猜</p>
    {{end}}
	</div>
</body>
<script src="/static/js/jquery-3.2.1.min.js"></script>
<script>
	function deviceMotionHandler(eventData) {
		var acceleration = eventData.accelerationIncludingGravity;
		var facingUp = -1;
		if (acceleration.z > 0) {
			facingUp = +1;
		}
		var tiltLR = Math.round(((acceleration.x) / 9.81) * -90);
		var tiltFB = Math.round(((acceleration.y + 9.81) / 9.81) * 90
				* facingUp);
		var rotation = "rotate(" + tiltLR + "deg) rotate3d(1,0,0, " + (tiltFB)
				+ "deg)";
		console.log(rotation);
		document.getElementById("blockImg").style.webkitTransform = rotation;
	}
	var body_ele = document.getElementById("body_ele");
	window.addEventListener("deviceorientation", function(event) {
		console.log(event);
		var alpha = event.alpha;
		var beta = event.beta;
		var gamma = event.gamma;
		var z_pos = beta / 2;
		var left_pos = alpha / 8;//左倾斜
		var right_pos = gamma / 2;//右边倾斜
		$(body_ele).attr(
				"style",
				"background: url(/static/images/Blocks.jpg); background-position:"
						+ right_pos + "% " + z_pos + "%");
		//mui.toast(alpha + "-" + beta + "-" + gamma);
	}, true);
</script>
</html>