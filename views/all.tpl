<!DOCTYPE html>
<html>

	<head>
		<meta charset="utf-8">
		<title>Elastos</title>
		<meta name="viewport" content="width=device-width, initial-scale=1,maximum-scale=1, user-scalable=no">
		<meta name="apple-mobile-web-app-capable" content="yes">
		<meta name="apple-mobile-web-app-status-bar-style" content="black">
		<link rel="stylesheet" href="/static/css/mui.min.css">
		<link rel="stylesheet" type="text/css" href="/static/css/base.css" />
		<script src="/static/js/mui.min.js"></script>
	</head>
	<style>
        .list_div {
            position: absolute;
            top: 10px;
            left: 0px;
            right: 0px;
            padding: 0px 15px;
        }

        .list_div p {
            margin-top: 16px;
        }

        .list_cell_content {
            border: 2px solid white;
            border-radius: 10px;
            padding: 8px 12px;
            position:relative;
            padding-right: 30px;
        }

        .list_cell_content p {
            margin: 0px;
            font-size: 14px;
        }

        .list_cell_content p:first-child {
            margin-bottom: 5px;
        }

        .list_cell_content p:last-child span {
            padding-left: 5px;
            margin-left: -3px;
        }
        .list_cell_content  img{
            position:absolute;
            right:15px;
            top:25px;
            width:13px;
        }
        .get span{
            margin-left: 15px;
        }
	</style>

	<body style="background: url(/static/images/Blocks.jpg);" id="body_ele">
		<div class="list_div">
			<img class="lootery" src="/static/images/Lottery.png" />
            {{range $i , $v := .all}}
			<div class="list_cell">
				<p>{{$v.update_time}} {{if eq ($v.reward | strToFloat)  0.0}}<span style="color: yellow">电影票已发送</span>{{end}}</p>
				<div class="list_cell_content">
					<a href="/lotterydetail/{{$v.height}}">上期区块#{{$v.height}} 末位字符：<span>{{$v.blockhash | lastStr | upper}}</span></a>
					<p>Hash值：{{$v.blockhash | trimLast}}<span class="color_get_yellow">{{$v.blockhash | lastStr | upper}}</span></p>
					<img src="/static/images/right.png"/>
				</div>
			</div>
            {{end}}
		</div>
	</body>
	<script src="/static/js/jquery-3.2.1.min.js"></script>

	<script>
		function deviceMotionHandler(eventData) {
			var acceleration = eventData.accelerationIncludingGravity;
			var facingUp = -1;
			if(acceleration.z > 0) {
				facingUp = +1;
			}
			var tiltLR = Math.round(((acceleration.x) / 9.81) * -90);
			var tiltFB = Math.round(((acceleration.y + 9.81) / 9.81) * 90 * facingUp);
			var rotation = "rotate(" + tiltLR + "deg) rotate3d(1,0,0, " + (tiltFB) + "deg)";
			console.log(rotation);
			document.getElementById("blockImg").style.webkitTransform = rotation;
		}
		var body_ele = document.getElementById("body_ele");
		window.addEventListener("deviceorientation", function(event) {
			console.log(event);
			var alpha = event.alpha;
			var beta = event.beta;
			var gamma = event.gamma;
			var z_pos=beta/2;
			var left_pos=alpha/8;//左倾斜
			var right_pos=gamma/2;//右边倾斜
			$(body_ele).attr("style", "background: url(/static/images/Blocks.jpg); background-position:" + right_pos + "% "+z_pos+"%");
			//mui.toast(alpha + "-" + beta + "-" + gamma);
		}, true);
	</script>
</html>