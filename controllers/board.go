package controllers

import (
	"Elastos.ORG.Lottery.Web/db"
	"encoding/json"
	"fmt"
	"github.com/astaxie/beego"
	"io/ioutil"
	"net/http"
	"strings"
	"time"
)

type BoardController struct {
	beego.Controller
}

func (this *BoardController) Get() {
	data := make([]map[string]string, 0)
	blockinfo := make(map[string]string)
	resp, err := http.Get(beego.AppConfig.String("GetBestHeight"))
	if err != nil {
		beego.Error(err)
		this.Data["msg"] = err.Error()
		this.TplName = "error.html"
		return
	}
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		beego.Error(err)
		this.Data["msg"] = err.Error()
		this.TplName = "error.html"
		return
	}
	result := make(map[string]interface{})
	json.Unmarshal(body, &result)
	height := fmt.Sprintf("%.0f", result["Result"].(float64))
	l, err := db.Dia.Query("select memo,blockhash,DATE_ADD(create_time,interval 8 HOUR) as create_time,height,txid from elastos_txblock where height = " + height)
	if err != nil {
		beego.Error(err)
		this.Data["msg"] = err.Error()
		this.TplName = "error.html"
		return
	}

	i := 0
	for e := l.Front(); e != nil; e = e.Next() {
		v := e.Value.(map[string]string)
		if i == 0 {
			blockinfo["blockhash"] = v["blockhash"]
			blockinfo["timestamp"] = v["create_time"]
			blockinfo["height"] = v["height"]
		}
		hash_memo := v["memo"]

		if strings.HasPrefix(hash_memo, "chinajoylottery") {
			m := make(map[string]string)
			m["txid"] = v["txid"]
			m["type"] = "猜数字上链"
			l, err = db.Dia.Query("select * from elastos_lottery_details a left join elastos_members b on a.openid = b.openid where a.txhash = '" + v["txid"] + "'")
			if err != nil || l.Len() == 0 {
				beego.Error(err, l.Len())
				continue
			}
			vv := l.Front().Value.(map[string]string)
			m["user"] = vv["nickname"]
			m["content"] = vv["hexChar"]
			data = append(data, m)
		} else if hash_memo == "chinajoy lottery wining reward" {
			m := make(map[string]string)
			m["txid"] = v["txid"]
			m["type"] = "猜数字中奖"
			l, err = db.Dia.Query("select * from elastos_lottery_details a left join elastos_members b on a.openid = b.openid  where a.reward_txhash = '" + v["txid"] + "'")
			if err != nil || l.Len() == 0 {
				beego.Error(err, l.Len())
				continue
			}
			vv := l.Front().Value.(map[string]string)
			m["user"] = vv["nickname"]
			m["content"] = vv["reward"] + "ELA"
			data = append(data, m)
		} else if hash_memo == "chinajoy registery reward" {
			l, err = db.Dia.Query("select * from elastos_register_details a left join elastos_members b on a.openid = b.openid where a.register_reward_tx = '" + v["txid"] + "'")
			if err != nil || l.Len() == 0 {
				beego.Error(err, l.Len())
				continue
			}
			for ie := l.Front(); ie != nil; ie = ie.Next() {
				m := make(map[string]string)
				m["txid"] = v["txid"]
				m["type"] = "注册空投"
				vv := ie.Value.(map[string]string)
				m["user"] = vv["nickname"]
				m["content"] = vv["register_reward"] + "ELA"
				data = append(data, m)
			}
		} else {
			m := make(map[string]string)
			m["txid"] = v["txid"]
			l, err = db.Dia.Query("select * from elastos_register_details a left join elastos_members b on a.openid = b.openid where a.register_info_tx = '" + v["txid"] + "'")
			if err != nil {
				beego.Error(err)
				continue
			}
			if l.Len() == 0 {
				l, err = db.Dia.Query("select * from elastos_joy_item a left join elastos_members b on b.user_id = a.user_id left join elastos_joy c on a.joy_id = c.id where a.blockhash = '" + hash_memo[8:] + "'")
				if err != nil {
					beego.Error(err)
					continue
				}
				if l.Len() == 0 {
					l, err = db.Dia.Query("select replace(raw_memo,'\n','') as raw_memo from elastos_transfer a where a.txid = '" + v["txid"] + "'")
					if err != nil || l.Len() == 0 {
						beego.Error(err)
						continue
					}
					m["type"] = "心动K歌"
					vv := l.Front().Value.(map[string]string)
					rawMemo := vv["raw_memo"]
					if strings.HasPrefix(rawMemo, "{") && strings.HasSuffix(rawMemo, "}") {
						var rawMap map[string]interface{}
						json.Unmarshal([]byte(rawMemo), &rawMap)
						m["content"] = "音乐名称：" + rawMap["music_name"].(string) + ",分数:" + fmt.Sprintf("%.0f", rawMap["score"])
						m["user"] = rawMap["role_name"].(string)
						data = append(data, m)
					}
				} else {
					vv := l.Front().Value.(map[string]string)
					m["type"] = vv["name"]
					score := vv["score"]
					m["content"] = "得分：" + score
					m["user"] = vv["nickname"]
					data = append(data, m)
				}
			} else {
				m["type"] = "注册信息"
				vv := l.Front().Value.(map[string]string)
				c := strings.Split(vv["register_memo_raw"], "&")
				m["content"] = c[0]
				m["user"] = vv["nickname"]
				data = append(data, m)
			}

		}

	}
	if l.Len() == 0 {
		blockinfo["blockhash"] = getHashByHeight(height)
		//t1, e := time.Parse(
		//	time.RFC3339,
		//	"2012-11-01T22:08:41+00:00")
		blockinfo["timestamp"] = time.Now().Add(time.Hour * 8).Format("2006-01-02 15:04:05")
		blockinfo["height"] = height
	}
	this.Data["blockinfo"] = blockinfo
	this.Data["txinfo"] = data
	this.TplName = "artboardlist.tpl"
}
