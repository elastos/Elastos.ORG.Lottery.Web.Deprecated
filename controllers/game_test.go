package controllers

import (
	"Elastos.ORG.Lottery.Web/db"
	"github.com/astaxie/beego"
	"os"
	"strconv"
	"testing"
)

func Test_go(t *testing.T) {
	list, err := db.Dia.Query("select * from elastos_lottery_details a left join elastos_txblock b on a.txhash = b.txid where a.isSend = 1 order by a.height desc limit 1")
	if err != nil {
		beego.Error(" init reward error ", err)
		os.Exit(-1)
	}
	v := list.Front().Value.(map[string]string)
	lastHeight, err := strconv.Atoi(v["height"])
	println(lastHeight)
}
