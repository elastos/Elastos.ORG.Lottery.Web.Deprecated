package controllers

import (
	"Elastos.ORG.Lottery.Web/db"
	"github.com/astaxie/beego"
)

type HomeController struct {
	beego.Controller
}

func (this *HomeController) Get() {
	openid := this.GetString(":openid")
	l, err := db.Dia.Query("select register_reward from elastos_register_details where openid = '" + openid + "'")
	if err != nil || l.Len() == 0 {
		beego.Error(" error ", err, " or can not find openid ")
		this.Data["msg"] = "Invalid request"
		this.TplName = "error.html"
		return
	}
	m := l.Front().Value.(map[string]string)
	if m["register_reward"] == "NULL" || m["register_reward"] == "" {
		this.Data["reward"] = "0"
		//this.Redirect("/registerUser/"+openid+"?"+strconv.Itoa(int(time.Now().Unix())), 302)
		//return
	} else {
		this.Data["reward"] = m["register_reward"]
	}
	l, err = db.Dia.Query("select movie_ticket,movie_ticket1 from elastos_lottery_details where openid = '" + openid + "' and movie_ticket is not null and status in (1,2)")
	if err != nil {
		beego.Error(err)
		this.Data["msg"] = "internal error"
		this.TplName = "error.html"
		return
	}
	beego.Info(l)
	ticArr := make([]map[string]string, 0)
	for e := l.Front(); e != nil; e = e.Next() {
		ticArr = append(ticArr, e.Value.(map[string]string))
	}
	this.Data["tickets"] = ticArr
	this.Data["openid"] = openid
	this.Data["registerWalletUrl"] = beego.AppConfig.String("registerWalletUrl")
	this.Data["gameAddr"] = beego.AppConfig.String("gameAddr")
	this.Data["lotteryAddr"] = beego.AppConfig.String("lotteryAddr")
	this.TplName = "main.tpl"
}
