package controllers

import (
	"Elastos.ORG.Lottery.Web/cron"
	"Elastos.ORG.Lottery.Web/db"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"github.com/astaxie/beego"
	"github.com/astaxie/beego/httplib"
	"github.com/elastos/Elastos.ELA.Utility/common"
	"io/ioutil"
	"log"
	"net/http"
	"strconv"
	"strings"
	"sync"
	"time"
)

var lock sync.Mutex

type IndexController struct {
	beego.Controller
}

type RetMsg struct {
	Result interface{}
	Error  int
	Desc   string
}

// @router / [get]
func (c *IndexController) Get() {
	openid := c.GetString("openid")
	isModify, err := c.GetInt("isModify")
	if err == nil && isModify == 1 {
		req := httplib.Get(beego.AppConfig.String("GET_VALIDATION_CODE_URL"))
		retStr, err := req.String()
		if err != nil {
			beego.Error("request validation code error")
			c.Data["msg"] = "Api internal Error"
			c.TplName = "error.html"
			return
		}
		retMap := make(map[string]interface{})
		err = json.Unmarshal([]byte(retStr), &retMap)
		if err != nil {
			beego.Error("request validation return result is not a json")
			c.Data["msg"] = "Api internal Error"
			c.TplName = "error.html"
			return
		}
		code := retMap["Result"].(map[string]interface{})["code"].(string)
		c.Data["openid"] = openid
		c.Data["vldCode"] = code
		c.Data["registed"] = 0
		l, err := db.Dia.Query("select wallet_addr from elastos_members where openid = '" + openid + "'")
		if err != nil || l.Len() == 0 {
			beego.Error(err, l)
			c.Data["msg"] = "no such user"
			c.TplName = "error.html"
			return
		}
		c.Data["addr"] = l.Front().Value.(map[string]string)["wallet_addr"]
		c.Data["registerWalletUrl"] = beego.AppConfig.String("registerWalletUrl")
		c.TplName = "welcome.tpl"
		return
	}
	vldCode := c.GetString("code")
	if openid == "" || vldCode == "" {
		beego.Error("openid || validation Code can not be blank ", openid, vldCode)
		c.Data["msg"] = "Invalid request"
		c.TplName = "error.html"
		return
	}
	l, err := db.Dia.Query("select * from elastos_register_details where openid = '" + openid + "'")
	if err != nil {
		beego.Error(" error ", err)
		c.Data["msg"] = err.Error()
		c.TplName = "error.html"
		return
	}

	if l.Len() > 0 {
		//model := l.Front().Value.(map[string]string)
		//c.Data["registed"] = 1
		//rewarded := model["status"]
		//if rewarded == "1" {
		c.Redirect("/home/"+openid+"?"+strconv.Itoa(int(time.Now().Unix())), 302)
		return
		//}
	} else {
		c.Data["registerWalletUrl"] = beego.AppConfig.String("registerWalletUrl")
		c.Data["registed"] = 0
	}
	c.Data["openid"] = openid
	c.Data["vldCode"] = vldCode
	c.TplName = "welcome.tpl"
}

func (this *IndexController) SubmitAddr() {

	addr := this.GetString(":addr")
	vldCode := this.GetString("vldCode")
	openid := this.GetString("openid")
	if addr == "" || vldCode == "" || openid == "" {
		beego.Error("addr or vldCode or openid can not be blank", addr, vldCode, openid)
		this.Data["json"] = RetMsg{Desc: "addr or vldCode or openid can not be blank", Error: -2}
		this.ServeJSON()
		return
	}

	if !checkAddr(addr) {
		beego.Error("address is not valid ", addr)
		this.Data["json"] = RetMsg{Desc: "address is not valid ", Error: -2}
		this.ServeJSON()
		return
	}

	l, err := db.Dia.Query("select * from elastos_members where openid = '" + openid + "'")
	if err != nil {
		beego.Error(" error ", err)
		this.Data["json"] = RetMsg{Desc: err.Error(), Error: -2}
		this.ServeJSON()
		return
	}
	if l.Len() == 0 {
		this.Data["json"] = RetMsg{Desc: "No such user", Error: -2}
		this.ServeJSON()
		return
	}
	model := l.Front().Value.(map[string]string)
	isExist := true
	if model["wallet_addr"] == "NULL" || model["wallet_addr"] == "" {
		isExist = false
	}
	// only check validation code when it is the first time binding address
	if isExist == false {
		l, err = db.Dia.Query("select * from elastos_info where vldCode = '" + vldCode + "'")
		if err != nil {
			beego.Error(" error ", err)
			this.Data["json"] = RetMsg{Desc: err.Error(), Error: -2}
			this.ServeJSON()
			return
		}
	}
	if l.Len() == 0 {
		beego.Error("validation code is outof data please Rescan the QR code")
		this.Data["json"] = RetMsg{Desc: "validation code is outof data please Rescan the QR code", Error: -2}
		this.ServeJSON()
		return
	}

	if model["wallet_addr"] == "" || model["wallet_addr"] == "NULL" {
		_, err = db.Dia.Exec("insert into elastos_register_details(openId) values('" + openid + "')")
		if err != nil {
			beego.Error(err.Error())
			this.Data["json"] = RetMsg{Desc: err.Error(), Error: -2}
			this.ServeJSON()
			return
		}
	}

	_, err = db.Dia.Exec("update elastos_members set wallet_addr = '" + addr + "' where openid = '" + openid + "'")
	if err != nil {
		beego.Error(err.Error())
		this.Data["json"] = RetMsg{Desc: err.Error(), Error: -2}
		this.ServeJSON()
		return
	}
	if isExist {
		this.Data["json"] = RetMsg{Error: 1}
		this.ServeJSON()
		//this.Redirect("/home/"+openid+"?"+strconv.Itoa(int(time.Now().Unix())), 302)
		return
	}
	//this.Data["registed"] = 1
	//this.Data["openid"] = model["openid"]
	//this.TplName = "index.tpl"
	this.Data["json"] = RetMsg{Error: 0}
	this.ServeJSON()
	return
}

var registerLock sync.Mutex

func (this *IndexController) RegisterUser() {

	openid := this.GetString(":openid")
	if openid == "" {
		beego.Error("openid can not be blank ", openid)
		this.Data["json"] = RetMsg{Error: -2}
		this.ServeJSON()
		return
	}
	l, err := db.Dia.Query("select * from elastos_register_details where openid = '" + openid + "' and status = 1")
	if err != nil {
		beego.Error(" error ", err)
		this.Data["json"] = RetMsg{Desc: err.Error(), Error: -2}
		this.ServeJSON()
		return
	}
	if l.Len() == 0 {
		this.Data["json"] = RetMsg{Desc: "can not find such user", Error: -2}
		this.ServeJSON()
		return
	}
	if l.Len() > 0 {
		this.Data["json"] = RetMsg{Error: 0}
		this.ServeJSON()
		return
	}
}

func checkAddr(walletAddr string) bool {
	uint168, err := common.Uint168FromAddress(walletAddr)
	if err != nil || (uint168[0] != 0x21 && uint168[0] != 0x12 && uint168[0] != 0x4B && uint168 != nil) {
		beego.Warn("illagal address %s , %s\n", walletAddr, err)
		return false
	}
	return true
}

func init() {
	// pre-register reward
	//PreRegisterInteval, err := beego.AppConfig.Int64("PreRegisterInteval")
	//if err != nil {
	//	beego.Error(err)
	//	os.Exit(-1)
	//}
	//go cron.AddScheduleBySec(PreRegisterInteval, func() {
	//	l, err := db.Dia.Query("select * from elastos_register_details a join elastos_members b on a.openId = b.openid where a.status = 0 and a.register_drop_tx is null and b.wallet_addr is not null")
	//	if err != nil || l.Len() == 0 {
	//		beego.Warn(" warn ", err, l)
	//		return
	//	}
	//	var receivInfo string
	//	var i int
	//	for e := l.Front(); e != nil; e = e.Next() {
	//		v := e.Value.(map[string]string)
	//
	//		if i == 0 && l.Len() != 1 {
	//			receivInfo += `[{"address":"` + v["wallet_addr"] + `","amount":"0.001"},`
	//		} else if i == 0 && l.Len() == 1 {
	//			receivInfo += `[{"address":"` + v["wallet_addr"] + `","amount":"0.001"}]`
	//		} else if i != l.Len()-1 {
	//			receivInfo += `{"address":"` + v["wallet_addr"] + `","amount":"0.001"},`
	//		} else {
	//			receivInfo += `{"address":"` + v["wallet_addr"] + `","amount":"0.001"}]`
	//		}
	//		i++
	//	}
	//
	//	body := `{
	//		"Action":"transfer",
	//		"Version":"1.0.0",
	//		"Data":
	//			{"senderAddr":"` + beego.AppConfig.String("RegisterPlateformAddr") + `",
	//			 "senderPrivateKey":"` + beego.AppConfig.String("RegisterPlateformPrivkey") + `",
	//			 "memo":"chinajoy registery reward",
	//			 "receiver":` + receivInfo + `
	//			}
	//		}`
	//	beego.Info("request body ", body)
	//	r := strings.NewReader(body)
	//	rsp, err := http.Post(beego.AppConfig.String("SendTransfer"), "application/json", r)
	//	if err != nil {
	//		beego.Error(err)
	//		return
	//	}
	//	bytes, err := ioutil.ReadAll(rsp.Body)
	//	if err != nil {
	//		beego.Error(err)
	//		return
	//	}
	//	beego.Debug("ret Msg : %s \n", string(bytes))
	//	var ret map[string]interface{}
	//	err = json.Unmarshal(bytes, &ret)
	//	if err != nil {
	//		beego.Error(err)
	//		return
	//	}
	//	if ret["error"].(float64) != 0 {
	//		return
	//	}
	//
	//	for e := l.Front(); e != nil; e = e.Next() {
	//		v := e.Value.(map[string]string)
	//		_, err = db.Dia.Exec("update elastos_register_details set register_reward = IFNULL(register_reward,0) + 0.001 , register_drop_tx = '" + ret["result"].(string) + "' where openid = '" + v["openId"] + "'")
	//		if err != nil {
	//			beego.Error("Error updating elastos_register_details register reward and tx ", err)
	//		}
	//	}
	//})

	// remove register airdrop for now
	//go cron.AddScheduleBySec(PreRegisterInteval,func() {
	//	l, err := db.Dia.Query("select * from elastos_register_details where status = 0")
	//	if err != nil {
	//		beego.Error(" error ", err)
	//		return
	//	}
	//	beego.Debug("register length is ",l.Len())
	//	var isSendReward bool
	//	for e:=l.Front() ; e != nil ; e = e.Next() {
	//		model := e.Value.(map[string]string)
	//		openid := model["openId"]
	//		beego.Debug("curr openid is ",openid)
	//		l, err = db.Dia.Query("select * from elastos_addresses where status = 1")
	//		if err != nil || l.Len() == 0 {
	//			beego.Error("initial address is not ready ", err, l.Len())
	//			continue
	//		}
	//		sendAddrModel := l.Front().Value.(map[string]string)
	//		l, err = db.Dia.Query("select wallet_addr from elastos_members where openid = '" + openid + "'")
	//		if err != nil || l.Len() == 0 {
	//			beego.Error("can not find suh member ", err, l.Len())
	//			continue
	//		}
	//		userwalletModel := l.Front().Value.(map[string]string)
	//
	//		if model["register_info_tx"] == "" || model["register_info_tx"] == "NULL" {
	//			raw := userwalletModel["wallet_addr"] + "&" + openid + "&" + strconv.Itoa(int(time.Now().Unix()))
	//			s := sha256.Sum256([]byte(raw))
	//			memo := "chinajoy" + hex.EncodeToString(s[:])
	//			body := `{
	//		"Action":"transfer",
	//		"Version":"1.0.0",
	//		"Data":
	//			{"senderAddr":"` + sendAddrModel["publicAddr"] + `",
	//			 "senderPrivateKey":"` + sendAddrModel["privKey"] + `",
	//			 "memo":"` + memo + `",
	//			 "receiver":[{"address":"` + beego.AppConfig.String("ReceivingPubAddr") + `","amount":"0.00000001"}]
	//			}
	//		}`
	//			beego.Info("request body ", body)
	//			r := strings.NewReader(body)
	//			rsp, err := http.Post(beego.AppConfig.String("SendTransfer"), "application/json", r)
	//			if err != nil {
	//				beego.Error(err)
	//				continue
	//			}
	//			bytes, err := ioutil.ReadAll(rsp.Body)
	//			if err != nil {
	//				beego.Error(err)
	//				continue
	//			}
	//			beego.Debug("ret Msg : %s \n", string(bytes))
	//			var ret map[string]interface{}
	//			err = json.Unmarshal(bytes, &ret)
	//			if err != nil {
	//				beego.Error(err)
	//				continue
	//			}
	//			if ret["error"].(float64) != 0 {
	//				continue
	//			}
	//			// update current active address
	//			_, err = db.Dia.Exec(" update elastos_addresses set status = 0 ,  spendTime = spendTime + 1 where status = 1")
	//			if err != nil {
	//				beego.Error(err)
	//				continue
	//			}
	//			id, err := db.Dia.Exec(" update elastos_addresses set status = 1 where id=" + sendAddrModel["id"] + "+1")
	//			if err != nil {
	//				beego.Error(err)
	//				continue
	//			} else if id == 0 {
	//				l, _ := db.Dia.Query("select id from elastos_addresses limit 1")
	//
	//				if l != nil && l.Len() > 0 {
	//					_, err := db.Dia.Exec(" update elastos_addresses set status = 1 where id = " + l.Front().Value.(map[string]string)["id"])
	//					if err != nil {
	//						beego.Error(err)
	//						continue
	//					}
	//				}
	//			}
	//
	//			_, err = db.Dia.Exec("update elastos_register_details set register_info_tx = '" + ret["result"].(string) + "', register_memo_hash = '" + memo + "',register_memo_raw='" + raw + "' where openid = '" + openid + "'")
	//			if err != nil {
	//				continue
	//			}
	//		} else if model["register_reward_tx"] == "" || model["register_reward_tx"] == "NULL" {
	//			if isSendReward {
	//				continue
	//			}
	//
	//			tx := model["register_info_tx"]
	//			l, err = db.Dia.Query("select * from (select * from elastos_txblock where height = ( select height from elastos_txblock where txid = '" + tx + "')) a left join elastos_register_details b on a.txid = b.register_info_tx left join elastos_members c on b.openId = c.openid where b.status = 0")
	//			if err != nil || l.Len() == 0 {
	//				continue
	//			}
	//			rand.Seed(time.Now().Unix())
	//			const ELA = 100000000
	//			const SELA = 0.00000001
	//			gape, _ := beego.AppConfig.Int("rewardRdmGape")
	//			base, _ := beego.AppConfig.Int("rewardRdmBase")
	//			regReward := math.Round(float64(rand.Int31n(int32(gape))+int32(base))*0.01*ELA/float64(l.Len())) * SELA
	//			var receivInfo string
	//			var i int
	//			for e := l.Front(); e != nil; e = e.Next() {
	//				v := e.Value.(map[string]string)
	//
	//				if i == 0 && l.Len() != 1 {
	//					receivInfo += `[{"address":"` + v["wallet_addr"] + `","amount":"` + fmt.Sprintf("%.8f", regReward) + `"},`
	//				} else if i == 0 && l.Len() == 1 {
	//					receivInfo += `[{"address":"` + v["wallet_addr"] + `","amount":"` + fmt.Sprintf("%.8f", regReward) + `"}]`
	//				} else if i != l.Len()-1 {
	//					receivInfo += `{"address":"` + v["wallet_addr"] + `","amount":"` + fmt.Sprintf("%.8f", regReward) + `"},`
	//				} else {
	//					receivInfo += `{"address":"` + v["wallet_addr"] + `","amount":"` + fmt.Sprintf("%.8f", regReward) + `"}]`
	//				}
	//				i++
	//			}
	//
	//			body := `{
	//		"Action":"transfer",
	//		"Version":"1.0.0",
	//		"Data":
	//			{"senderAddr":"` + beego.AppConfig.String("PlateformAddr") + `",
	//			 "senderPrivateKey":"` + beego.AppConfig.String("PlateformPrivkey") + `",
	//			 "memo":"chinajoy registery reward",
	//			 "receiver":` + receivInfo + `
	//			}
	//		}`
	//			beego.Info("request body ", body)
	//			r := strings.NewReader(body)
	//			rsp, err := http.Post(beego.AppConfig.String("SendTransfer"), "application/json", r)
	//			if err != nil {
	//				beego.Error(err)
	//				continue
	//			}
	//			bytes, err := ioutil.ReadAll(rsp.Body)
	//			if err != nil {
	//				beego.Error(err)
	//				continue
	//			}
	//			beego.Debug("ret Msg : %s \n", string(bytes))
	//			var ret map[string]interface{}
	//			err = json.Unmarshal(bytes, &ret)
	//			if err != nil {
	//				beego.Error(err)
	//				continue
	//			}
	//			if ret["error"].(float64) != 0 {
	//				continue
	//			}
	//
	//			for e := l.Front(); e != nil; e = e.Next() {
	//				v := e.Value.(map[string]string)
	//				_, err = db.Dia.Exec("update elastos_register_details set register_reward = IFNULL(register_reward,0) + " + fmt.Sprintf("%.8f", regReward) + " , register_reward_tx = '" + ret["result"].(string) + "' where openid = '" + v["openId"] + "'")
	//				if err != nil {
	//					beego.Error("Error updating elastos_register_details register reward and tx ", err)
	//				}
	//			}
	//
	//			if err != nil {
	//				beego.Error(err)
	//				continue
	//			}
	//			isSendReward = true
	//		} else {
	//			if i, err := strconv.Atoi(model["status"]); err != nil && i == 1 {
	//				continue
	//			}
	//
	//			tx := model["register_reward_tx"]
	//			req := httplib.Get(beego.AppConfig.String("GetTransactionByHash") + "/" + tx)
	//			ret := make(map[string]interface{})
	//			err := req.ToJSON(&ret)
	//			beego.Info("getTxByhash ret ", ret)
	//			if err != nil {
	//				continue
	//			}
	//			if ret["Error"].(float64) == 0 {
	//				// update status of register details
	//				_, err = db.Dia.Exec("update elastos_register_details set status = 1 where register_reward_tx = '" + tx + "'")
	//				if err != nil {
	//					beego.Error(err)
	//					continue
	//				}
	//			}
	//		}
	//	}
	//	beego.Debug("done")
	//})

	go cron.AddScheduleBySec(10,
		func() {
			txlock.Lock()
			defer txlock.Unlock()
			_, err := processTx()
			if err != nil {
				beego.Error("process sync_block error ", err)
				return
			}
		})

}

var txlock sync.Mutex
var syncHeight = 0
var InitialHeight, _ = beego.AppConfig.Int("InitialHeight")
var GetBestHeight = beego.AppConfig.String("GetBestHeight")
var GetBlockByHeight = beego.AppConfig.String("GetBlockByHeight")

func processTx() (bool, error) {
	var start int
	var end int

	resp, err := http.Get(GetBestHeight)
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return false, err
	}
	result := make(map[string]interface{})
	json.Unmarshal(body, &result)
	end = int(result["Result"].(float64))
	if syncHeight == 0 {
		list, err := db.Dia.Query(" select height from elastos_txblock order by height desc limit 1")
		if err != nil {
			return false, err
		}
		if list.Len() == 0 {
			start = int(InitialHeight)
		} else {
			m := list.Front().Value.(map[string]string)
			startStr := m["height"]
			start, err = strconv.Atoi(startStr)
			if err != nil {
				return false, err
			}
			if start < int(InitialHeight) {
				start = int(InitialHeight)
			} else {
				start = start + 1
			}
		}
	} else {
		start = syncHeight + 1
	}
	if start >= end+1 {
		log.Println("no block need to sync")
		return true, nil
	}
	for height := start; height < end+1; height++ {
		log.Printf("sync height : %d \n", height)
		resp, err := http.Get(GetBlockByHeight + "/" + strconv.Itoa(height))
		if err != nil {
			return false, err
		}
		body, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			return false, err
		}
		var blockInfo map[string]interface{}
		json.Unmarshal(body, &blockInfo)
		rstMap := blockInfo["Result"].(map[string]interface{})
		blockHash := rstMap["hash"].(string)
		timestamp := fmt.Sprintf("%.0f", rstMap["time"])
		txArr := rstMap["tx"].([]interface{})
		blockHeight := int(rstMap["height"].(float64))
		for i := 0; i < len(txArr); i++ {
			txInfo := txArr[i].(map[string]interface{})
			txId := txInfo["txid"].(string)
			if int(txInfo["type"].(float64)) != 2 {
				continue
			}
			attributes := txInfo["attributes"].([]interface{})
			if len(attributes) == 0 {
				continue
			}
			memoByte, err := hex.DecodeString(attributes[0].(map[string]interface{})["data"].(string))
			if err != nil {
				return false, err
			}
			memo := string(memoByte)
			log.Printf("memo info %s \n", memo)
			if !strings.HasPrefix(memo, "chinajoy") {
				continue
			}

			//update joy
			_, err = db.Dia.Exec("update elastos_joy_item set txId = '" + txId + "' where blockhash = '" + memo[8:] + "'")
			if err != nil {
				return false, err
			}
			// check lottery
			if strings.HasPrefix(memo, "chinajoylottery") {

				l, err := db.Dia.Query("select * from elastos_lottery_details where txhash = '" + txId + "'")
				if err != nil {
					beego.Error(err)
					continue
				}
				if l.Len() == 0 {
					info := strings.Split(memo, "@")
					if len(info) >= 4 && checkAmtAndAddr(txInfo) {
						hexChar := info[1]
						if len(hexChar) == 1 && (([]byte(hexChar)[0] >= 48 && []byte(hexChar)[0] <= 57) || ([]byte(hexChar)[0] >= 97 && []byte(hexChar)[0] <= 102)) {
							openid := info[3]
							//prevent sql injection
							if !strings.Contains(openid, "\"") {
								l, err := db.Dia.Query("select * from elastos_members where openid = '" + openid + "'")
								if err != nil || l.Len() == 0 {
									beego.Warn(err, l.Len())
									continue
								}

								beego.Debug("hexChar,blockHash,openid,currHeight", hexChar, blockHash, openid, R.CurrBlock)

								//l, err = db.Dia.Query("select * from elastos_lottery_details where openid = '" + openid + "' and height = " + strconv.Itoa(R.CurrBlock))
								//
								//if l.Len() > 0 || err != nil {
								//	beego.Warn(err, l.Len())
								//	continue
								//}

								_, err = db.Dia.Exec("insert into elastos_lottery_details(openid,hexChar,txhash,height) values('" + openid + "','" + hexChar + "','" + txId + "'," + strconv.Itoa(R.CurrBlock) + ")")
								if err != nil {
									beego.Error(err)
									continue
								}
							}
						}
					}
					// add trinity lottery game support
					// chinajoylottery-a-EbCarQQTxYAp1MBfPjXZorF4FTgsqJaK5n-appSecret
					if len(attributes) >= 2 {
						memoByte, err := hex.DecodeString(attributes[1].(map[string]interface{})["data"].(string))
						if err != nil {
							return false, err
						}
						memo := string(memoByte)
						info = strings.Split(memo, "-")
						if len(info) >= 4 {
							hexChar := info[1]
							beego.Debug("memo,blockHash,currHeight", memo, blockHash, R.CurrBlock)
							if len(hexChar) == 1 && (([]byte(hexChar)[0] >= 48 && []byte(hexChar)[0] <= 57) || ([]byte(hexChar)[0] >= 97 && []byte(hexChar)[0] <= 102)) {
								DID := info[2]
								if !strings.Contains(DID, "\"") {
									if beego.AppConfig.String("TrinitySecret") == info[3] {
										l, err := db.Dia.Query("select * from elastos_members where openid = '" + DID + "'")

										if err != nil {
											beego.Warn(err, l.Len())
											continue
										}
										if l.Len() == 0 {
											trinityMockName := "trinity" + DID
											trinityMockOpenid := DID
											trinityMockImg := beego.AppConfig.String("TrinityIcon")
											vin := txInfo["vin"].([]interface{})
											vinTx := vin[0].(map[string]interface{})["txid"]
											vinIndex := vin[0].(map[string]interface{})["vout"]
											resp , err := http.Get(beego.AppConfig.String("GetTransactionByHash")+"/"+vinTx.(string))
											if err != nil {
												beego.Error(err.Error())
												continue
											}
											buf , err := ioutil.ReadAll(resp.Body)
											outTx := make(map[string]interface{})
											err = json.Unmarshal(buf,&outTx)
											if err != nil {
												beego.Error(err.Error())
												continue
											}
											addr := outTx["Result"].(map[string]interface{})["vout"].([]interface{})[int(vinIndex.(float64))].(map[string]interface{})["address"]
											if err != nil {
												beego.Error(err.Error())
												continue
											}

											_, err = db.Dia.Exec("insert into elastos_members(nickname,openid,headimgurl,wallet_addr) values('" + trinityMockName + "','" + trinityMockOpenid + "','" + trinityMockImg + "','" + addr.(string) + "')")
											if err != nil {
												beego.Error(err)
												continue
											}

											_, err = db.Dia.Exec("insert into elastos_lottery_details(openid,hexChar,txhash,height) values('" + trinityMockOpenid + "','" + hexChar + "','" + txId + "'," + strconv.Itoa(R.CurrBlock) + ")")
											if err != nil {
												beego.Error(err)
												continue
											}
										} else {
											openid := l.Front().Value.(map[string]string)["openid"]

											beego.Debug("openid ", openid)

											l, err = db.Dia.Query("select * from elastos_lottery_details where openid = '" + openid + "' and height = " + strconv.Itoa(R.CurrBlock))

											if l.Len() > 0 || err != nil {
												beego.Warn(err, l.Len())
												continue
											}

											_, err = db.Dia.Exec("insert into elastos_lottery_details(openid,hexChar,txhash,height) values('" + openid + "','" + hexChar + "','" + txId + "'," + strconv.Itoa(R.CurrBlock) + ")")
											if err != nil {
												beego.Error(err)
												continue
											}
										}
									}
								}
							}
						}
					}
				}
			}
			// put the tx sync at the bottom
			sql := "insert into elastos_txblock (txid,height,memo,timestamp,blockhash) values('" + txId + "'," + strconv.Itoa(blockHeight) + ",'" + memo + "'," + timestamp + ",'" + blockHash + "')"
			_, err = db.Dia.Exec(sql)
			if err != nil {
				return false, err
			}
		}
		if height == end && height > syncHeight {
			syncHeight = height
		}
	}

	return true, nil
}

// only used for movie ticket lottery
func checkAmtAndAddr(txInfo map[string]interface{}) bool {
	beego.Info("txinfo : ", txInfo)
	voutArr := txInfo["vout"].([]interface{})
	for _, v := range voutArr {
		vi := v.(map[string]interface{})
		movieTicketPrice, err := beego.AppConfig.Float("MovieTicketPrice")
		if err != nil {
			beego.Error(err)
			return false
		}
		amount := movieTicketPrice / 100000000
		cAmt, err := strconv.ParseFloat(vi["value"].(string), 64)
		if err != nil {
			beego.Error(err)
			return false
		}

		recAddr := beego.AppConfig.String("ReceivingPubAddr")
		beego.Info("amount , cAmt , recArr , vi ", amount, cAmt, recAddr, vi["address"])
		if amount == cAmt && recAddr == vi["address"] {
			return true
		}
	}
	return false
}
