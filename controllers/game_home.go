package controllers

import (
	"Elastos.ORG.Lottery.Web/cron"
	"Elastos.ORG.Lottery.Web/db"
	"encoding/json"
	"fmt"
	"github.com/astaxie/beego"
	"github.com/astaxie/beego/httplib"
	"io/ioutil"
	"net/http"
	"os"
	"strconv"
	"strings"
	"sync"
)

var R Reward
var rwLock = sync.RWMutex{}

type Reward struct {
	LastBlock     int
	LastBlockHash string
	CurrBlock     int
	CurrBlockHash string
}

type GameController struct {
	beego.Controller
}

func (this *GameController) Get() {
	beego.Debug(" the current block state is ", R)
	openid := this.GetString("openid")
	l, err := db.Dia.Query("select * from elastos_members where openid = '" + openid + "'")
	if err != nil || l.Len() == 0 {
		beego.Error(err)
		this.Data["msg"] = "Internal Error"
		this.TplName = "error.html"
		return
	}
	v := l.Front().Value.(map[string]string)
	wallet := v["wallet_addr"]
	if wallet == "" || wallet == "NULL" {
		beego.Error("wallet is blank")
		this.Data["msg"] = "please bind your ela wallet first"
		this.TplName = "error.html"
		return
	}

	this.Data["walletAddr"] = beego.AppConfig.String("ReceivingPubAddr")
	this.Data["blockReward"] = R
	this.Data["returlUrl"] = beego.AppConfig.String("ReturnUrl")
	this.Data["CallbackUrl"] = beego.AppConfig.String("CallbackUrl")
	this.Data["net"] = beego.AppConfig.String("net")
	this.Data["openid"] = openid
	this.Data["sendEla"] = beego.AppConfig.String("MovieTicketPrice")
	this.TplName = "game_home.tpl"
}

func init() {
	R = Reward{}
	bginteval, err := strconv.Atoi(beego.AppConfig.String("BackGroudJobCheckInteval"))
	inteval, err := strconv.Atoi(beego.AppConfig.String("LotteryInterval"))
	list, err := db.Dia.Query("select a.height from elastos_lottery_details a left join elastos_txblock b on a.txhash = b.txid where a.isSend = 1 order by a.height desc limit 1")
	if err != nil {
		beego.Error(" init reward error ", err)
		os.Exit(-1)
	}
	req := httplib.Get(beego.AppConfig.String("GetBestHeight"))
	respStr, err := req.String()
	if err != nil {
		beego.Error(err)
		os.Exit(-1)
	}
	var retMap map[string]interface{}
	err = json.Unmarshal([]byte(respStr), &retMap)
	if err != nil {
		beego.Error(err)
		os.Exit(-1)
	}
	currBestHeight := int(retMap["Result"].(float64))
	if list.Len() == 0 {
		hash := getHashByHeight(strconv.Itoa(currBestHeight))
		R.LastBlock = currBestHeight
		R.CurrBlock = R.LastBlock + inteval
		R.LastBlockHash = hash
		if err != nil {
			beego.Error(err)
			os.Exit(-1)
		}
	} else {
		v := list.Front().Value.(map[string]string)
		lastHeight, err := strconv.Atoi(v["height"])
		if err != nil {
			beego.Error(err)
			os.Exit(-1)
		}

		if lastHeight+inteval <= currBestHeight {
			R.LastBlock = currBestHeight
		} else {
			R.LastBlock = lastHeight
		}
		hash := getHashByHeight(strconv.Itoa(R.LastBlock))
		R.LastBlockHash = hash
		if err != nil {
			beego.Error(err)
			os.Exit(-1)
		}
		R.CurrBlock = R.LastBlock + inteval
	}

	go cron.AddScheduleBySec(int64(bginteval), func() {

		rwLock.Lock()
		defer rwLock.Unlock()
		txlock.Lock()
		defer txlock.Unlock()
		l, err := db.Dia.Query("select a.* from elastos_lottery_details a where a.isSend = 0 and exists(select * from elastos_txblock where txid = a.txhash) order by height ")
		if err != nil {
			beego.Error(err)
			return
		}
		if l.Len() > 0 {
			m := l.Front().Value.(map[string]string)
			height := m["height"]
			hash := getHashByHeight(height)
			if hash != "" {
				var refundReady bool
				var sendTicketReady bool
				lucyHex := hash[len(hash)-1:]
				_, err = db.Dia.Exec("update elastos_lottery_details set status = 1 where height = " + height + " and hexChar = '" + lucyHex + "'")
				if err != nil {
					beego.Error(err)
					return
				}
				_, err = db.Dia.Exec("update elastos_lottery_details set blockhash = '" + hash + "' where height = " + height)
				if err != nil {
					beego.Error(err)
					return
				}

				// sending tickets
				inl, err := db.Dia.Query("select * from elastos_lottery_details where status = 1 and height = " + height)
				if err != nil {
					beego.Error(err)
					return
				}
				if inl.Len() == 0 {
					sendTicketReady = true
				} else {
					sendTicketReady = true
					isAlreadySendTicktets, err := db.Dia.Query("select * from elastos_lottery_details where status = 1 and movie_ticket is not null and  height = " + height + " and openid = '" + inl.Front().Value.(map[string]string)["openid"] + "'")
					if isAlreadySendTicktets.Len() == 0 {
						for v := inl.Front(); v != nil; v = v.Next() {
							tics, err := db.Dia.Query("select MovieCode from elastos_movie_code where status = 0 order by id limit 2")
							if tics.Len() == 2 {
								v1 := tics.Front().Value.(map[string]string)
								MovieCode1 := v1["MovieCode"]
								v2 := tics.Front().Next().Value.(map[string]string)
								MovieCode2 := v2["MovieCode"]
								_, err = db.Dia.Exec("update elastos_lottery_details set movie_ticket = '" + MovieCode1 + "',movie_ticket1='" + MovieCode2 + "' where status = 1 and height = " + height + " and openid = '" + v.Value.(map[string]string)["openid"] + "' and id = " + v.Value.(map[string]string)["id"])
								if err != nil {
									beego.Info(err.Error())
									sendTicketReady = false
								}
								_, err = db.Dia.Exec("update elastos_movie_code set status = 1 where MovieCode in ('" + MovieCode1 + "','" + MovieCode2 + "')")
								if err != nil {
									beego.Info(err.Error())
									sendTicketReady = false
								}
							} else {
								_, err = db.Dia.Exec("update elastos_lottery_details set movie_ticket = 'Out Of Tickets, Sorry',movie_ticket1 = 'Out Of Tickets, Sorry', status = 2 where status = 1 and height = " + height + " and openid = '" + v.Value.(map[string]string)["openid"] + "' and id = " + v.Value.(map[string]string)["id"])
								if err != nil {
									beego.Info(err.Error())
									sendTicketReady = false
								}
							}
						}

						if sendTicketReady {
							_, err = db.Dia.Exec("update elastos_lottery_details set isSend = 1 where height = " + height + " and status = 1")
							if err != nil {
								beego.Error(err)
								return
							}
						}
					}
				}

				// send back their money
				inl, err = db.Dia.Query("select * from elastos_lottery_details where status in (0,2) and isSend = 0 and height = " + height)
				if err != nil {
					beego.Error(err)
					return
				}
				len := inl.Len()
				if len > 0 {
					var receivInfo string
					var i int
					movieTicketPrice, err := beego.AppConfig.Float("MovieTicketPrice")
					if err != nil {
						beego.Error("lottery reward config setting error ", err)
						os.Exit(-1)
					}
					amount := movieTicketPrice / 100000000
					for e := inl.Front(); e != nil; e = e.Next() {
						v := e.Value.(map[string]string)
						openid := v["openid"]
						mem, err := db.Dia.Query("select * from elastos_members where openid = '" + openid + "'")
						if err != nil || mem.Len() == 0 {
							continue
						}
						walletAddr := mem.Front().Value.(map[string]string)["wallet_addr"]
						if i == 0 && len == 1 {
							receivInfo += `[{"address":"` + walletAddr + `","amount":"` + fmt.Sprintf("%.8f", amount) + `"}]`
						} else if i == 0 && len > 1 {
							receivInfo += `[{"address":"` + walletAddr + `","amount":"` + fmt.Sprintf("%.8f", amount) + `"},`
						} else if i != len-1 {
							receivInfo += `{"address":"` + walletAddr + `","amount":"` + fmt.Sprintf("%.8f", amount) + `"},`
						} else {
							receivInfo += `{"address":"` + walletAddr + `","amount":"` + fmt.Sprintf("%.8f", amount) + `"}]`
						}
						i++
					}
					body :=
						`{"Action":"transfer",
			"Version":"1.0.0",
			"Data":
				{"senderAddr":"` + beego.AppConfig.String("PlateformAddr") + `",
   				 "senderPrivateKey":"` + beego.AppConfig.String("PlateformPrivkey") + `",
				 "memo":"chinajoy lottery wining reward",
				 "receiver":` + receivInfo + `
				}
			}`
					beego.Info("request info ", body)
					r := strings.NewReader(body)

					rsp, err := http.Post(beego.AppConfig.String("SendTransfer"), "application/json", r)
					if err != nil {
						beego.Error(err)
						return
					}
					bytes, err := ioutil.ReadAll(rsp.Body)
					if err != nil {
						beego.Error(err)
						return
					}
					beego.Debug("ret Msg : %s \n", string(bytes))
					var ret map[string]interface{}
					err = json.Unmarshal(bytes, &ret)
					if err != nil {
						beego.Error(err)
						return
					}
					if ret["error"].(float64) != 0 {
						beego.Error("send fee Error")
						return
					} else {
						_, err = db.Dia.Exec("update elastos_lottery_details set reward_txhash = '" + ret["result"].(string) + "' ,reward = " + fmt.Sprintf("%.8f", amount) + " where height = " + height + " and status = 0")
						if err != nil {
							beego.Error(err)
							return
						}
						_, err = db.Dia.Exec("update elastos_lottery_details set isSend = 1 where height = " + height + " and status in (0,2)")
						if err != nil {
							beego.Error(err)
							return
						}
						refundReady = true
					}
				}


				if refundReady && sendTicketReady {
					h, err := strconv.Atoi(height)
					if err == nil && h == R.CurrBlock {
						R.LastBlockHash = getHashByHeight(height)
						R.LastBlock = R.CurrBlock
						R.CurrBlock = R.LastBlock + inteval
						return
					}
				}
			}
		}

		// check if nobody play the lottery game . then uplift the currentHeight
		beego.Debug("get the current bestHeight")
		req := httplib.Get(beego.AppConfig.String("GetBestHeight"))
		respStr, err := req.String()
		if err != nil {
			beego.Error(err)
			os.Exit(-1)
		}
		var retMap map[string]interface{}
		err = json.Unmarshal([]byte(respStr), &retMap)
		if err != nil {
			beego.Error(err)
			os.Exit(-1)
		}
		currBestHeight := int(retMap["Result"].(float64))

		if currBestHeight >= R.LastBlock+inteval {
			beego.Debug("adjust the last height to ", currBestHeight)
			R.LastBlock = currBestHeight
			R.LastBlockHash = getHashByHeight(strconv.Itoa(R.LastBlock))
			R.CurrBlock = R.LastBlock + inteval
		}

		if currBestHeight >= R.CurrBlock {
			beego.Debug("adjust the current height to ", R.LastBlock+inteval)
			R.CurrBlock = R.LastBlock + inteval
		}
	})

	//go cron.AddScheduleBySec(int64(bginteval), func() {
	//
	//	rwLock.Lock()
	//	defer rwLock.Unlock()
	//
	//	l, err := db.Dia.Query("select a.* from elastos_lottery_details a where a.isSend = 0 and exists(select * from elastos_txblock where txid = a.txhash) order by height ")
	//	if err != nil {
	//		beego.Error(err)
	//		return
	//	}
	//	if l.Len() > 0 {
	//		m := l.Front().Value.(map[string]string)
	//		height := m["height"]
	//		hash := getHashByHeight(height)
	//		if hash != "" {
	//			lucyHex := hash[len(hash)-1:]
	//			_, err = db.Dia.Exec("update elastos_lottery_details set status = 1 where height = " + height + " and hexChar = '" + lucyHex + "'")
	//			if err != nil {
	//				beego.Error(err)
	//				return
	//			}
	//			_, err = db.Dia.Exec("update elastos_lottery_details set blockhash = '" + hash + "' where height = " + height)
	//			if err != nil {
	//				beego.Error(err)
	//				return
	//			}
	//			inl, err := db.Dia.Query("select * from elastos_lottery_details where status = 1 and height = " + height)
	//			if err != nil {
	//				beego.Error(err)
	//				return
	//			}
	//			if inl.Len() == 0 {
	//				_, err = db.Dia.Exec("update elastos_lottery_details set isSend = 1 where height = " + height)
	//				if err != nil {
	//					beego.Error(err)
	//					return
	//				}
	//				h, err := strconv.Atoi(height)
	//				if err == nil && h == R.CurrBlock {
	//					R.LastBlockHash = getHashByHeight(height)
	//					R.LastBlock = R.CurrBlock
	//					R.CurrBlock = R.LastBlock + inteval
	//				}
	//				return
	//			}
	//
	//			var receivInfo string
	//			len := inl.Len()
	//			var i int
	//			totalReward , err := beego.AppConfig.Float("LotteryReward")
	//			if err != nil {
	//				beego.Error("lottery reward config setting error ",err)
	//				os.Exit(-1)
	//			}
	//			amount := math.Round(totalReward/float64(inl.Len())) / 100000000
	//			for e := inl.Front(); e != nil; e = e.Next() {
	//				v := e.Value.(map[string]string)
	//				openid := v["openid"]
	//				mem, err := db.Dia.Query("select * from elastos_members where openid = '" + openid + "'")
	//				if err != nil || mem.Len() == 0 {
	//					continue
	//				}
	//				walletAddr := mem.Front().Value.(map[string]string)["wallet_addr"]
	//				if i == 0 && len == 1 {
	//					receivInfo += `[{"address":"` + walletAddr + `","amount":"` + fmt.Sprintf("%.8f", amount) + `"}]`
	//				} else if i == 0 && len > 1 {
	//					receivInfo += `[{"address":"` + walletAddr + `","amount":"` + fmt.Sprintf("%.8f", amount) + `"},`
	//				} else if i != len-1 {
	//					receivInfo += `{"address":"` + walletAddr + `","amount":"` + fmt.Sprintf("%.8f", amount) + `"},`
	//				} else {
	//					receivInfo += `{"address":"` + walletAddr + `","amount":"` + fmt.Sprintf("%.8f", amount) + `"}]`
	//				}
	//				i++
	//			}
	//			body := `{
	//		"Action":"transfer",
	//		"Version":"1.0.0",
	//		"Data":
	//			{"senderAddr":"` + beego.AppConfig.String("PlateformAddr") + `",
	//			 "senderPrivateKey":"` + beego.AppConfig.String("PlateformPrivkey") + `",
	//			 "memo":"chinajoy lottery wining reward",
	//			 "receiver":` + receivInfo + `
	//			}
	//		}`
	//			beego.Info("request info ", body)
	//			r := strings.NewReader(body)
	//
	//			rsp, err := http.Post(beego.AppConfig.String("SendTransfer"), "application/json", r)
	//			if err != nil {
	//				beego.Error(err)
	//				return
	//			}
	//			bytes, err := ioutil.ReadAll(rsp.Body)
	//			if err != nil {
	//				beego.Error(err)
	//				return
	//			}
	//			beego.Debug("ret Msg : %s \n", string(bytes))
	//			var ret map[string]interface{}
	//			err = json.Unmarshal(bytes, &ret)
	//			if err != nil {
	//				beego.Error(err)
	//				return
	//			}
	//			if ret["error"].(float64) != 0 {
	//				beego.Error("send fee Error")
	//				return
	//			} else {
	//
	//				_, err = db.Dia.Exec("update elastos_lottery_details set reward_txhash = '" + ret["result"].(string) + "' ,reward = " + fmt.Sprintf("%.8f", amount) + " where height = " + height + " and status = 1")
	//				if err != nil {
	//					beego.Error(err)
	//					return
	//				}
	//				_, err = db.Dia.Exec("update elastos_lottery_details set isSend = 1 where height = " + height)
	//				if err != nil {
	//					beego.Error(err)
	//					return
	//				}
	//				h, err := strconv.Atoi(height)
	//				if err == nil && h == R.CurrBlock {
	//					R.LastBlockHash = getHashByHeight(height)
	//					R.LastBlock = R.CurrBlock
	//					R.CurrBlock = R.LastBlock + inteval
	//					return
	//				}
	//			}
	//		}
	//	}
	//
	//	// check if nobody play the lottery game . then uplift the currentHeight
	//	beego.Debug("get the current bestHeight")
	//	req := httplib.Get(beego.AppConfig.String("GetBestHeight"))
	//	respStr, err := req.String()
	//	if err != nil {
	//		beego.Error(err)
	//		os.Exit(-1)
	//	}
	//	var retMap map[string]interface{}
	//	err = json.Unmarshal([]byte(respStr), &retMap)
	//	if err != nil {
	//		beego.Error(err)
	//		os.Exit(-1)
	//	}
	//	currBestHeight := int(retMap["Result"].(float64))
	//
	//	if currBestHeight >= R.LastBlock+inteval {
	//		beego.Debug("adjust the last height to ", currBestHeight)
	//		R.LastBlock = currBestHeight
	//		R.LastBlockHash = getHashByHeight(strconv.Itoa(R.LastBlock))
	//	}
	//
	//	if currBestHeight >= R.CurrBlock {
	//		beego.Debug("adjust the current height to ", R.LastBlock+inteval)
	//		R.CurrBlock = R.LastBlock + inteval
	//	}
	//})
}

func (this *GameController) SubmitAnswer() {
	beego.Warn("have to get data from the chain")
	this.Data["json"] = "FAIL"
	this.ServeJSON()
	return
	//all := this.GetString("openid")
	//if all == "" {
	//	beego.Error(" Error callback ", all)
	//	this.Data["json"] = "FAIL"
	//	this.ServeJSON()
	//	return
	//}
	//
	//openid := all[:len(all)-1]
	//answer := all[len(all)-1:]
	//txhash := this.GetString("txhash")
	//
	//if openid == "" || answer == "" || txhash == "" {
	//	beego.Error(" Error callback ", openid, answer, txhash)
	//	this.Data["json"] = "FAIL"
	//	this.ServeJSON()
	//	return
	//}
	//l, err := db.Dia.Query("select * from elastos_lottery_details where openid = '" + openid + "' and height = " + strconv.Itoa(R.CurrBlock))
	//if err != nil || l.Len() > 1 {
	//	beego.Error(err, l.Len())
	//	this.Data["json"] = "already vote"
	//	this.ServeJSON()
	//	return
	//}
	//rwLock.RLock()
	//defer rwLock.RUnlock()
	//_, err = db.Dia.Exec("insert into elastos_lottery_details(openid,hexChar,txhash,height) values('" + openid + "','" + answer + "','" + txhash + "'," + strconv.Itoa(R.CurrBlock) + ")")
	//if err != nil {
	//	beego.Error(err)
	//	this.Data["json"] = "FAIL"
	//	this.ServeJSON()
	//	return
	//}
	//this.Data["json"] = "SUCC"
	//this.ServeJSON()
}

func (this *GameController) IsGuessed() {
	openid := this.GetString("openid")
	l, err := db.Dia.Query("select * from elastos_lottery_details where height = " + strconv.Itoa(R.CurrBlock) + " and openid = '" + openid + "'")
	if err != nil || l.Len() > 0 {
		beego.Error(err, l.Len())
		this.Data["json"] = RetMsg{Error: -2}
		this.ServeJSON()
		return
	}
	l, err = db.Dia.Query("select * from elastos_lottery_details where status = 1 and openid = '" + openid + "' and movie_ticket is not null ")
	if err != nil || l.Len() > 0 {
		beego.Error(err, l.Len())
		this.Data["json"] = RetMsg{Error: -3}
		this.ServeJSON()
		return
	}
	this.Data["json"] = RetMsg{Error: 0}
	this.ServeJSON()
	return
}

func (this *GameController) Detail() {
	height := this.GetString(":height")
	if height == "" {
		beego.Error("height is blank")
		this.Data["msg"] = "invalid request"
		this.TplName = "error.html"
		return
	}
	l, err := db.Dia.Query("select status,hexChar,openid,height,blockhash,DATE_ADD(update_time,interval 8 HOUR) as update_time,DATE_ADD(create_time,interval 8 HOUR) as create_time from elastos_lottery_details where height = " + height)
	if err != nil {
		beego.Error(err)
	}
	mArr := make([]map[string]string, l.Len())
	var i int
	for e := l.Front(); e != nil; e = e.Next() {
		m := e.Value.(map[string]string)
		tmpl, err := db.Dia.Query("select * from elastos_members where openid = '" + m["openid"] + "'")
		if err != nil || tmpl.Len() == 0 {
			beego.Error(err)
			this.Data["msg"] = "No such openid"
			this.TplName = "error.html"
			return
		}
		nickname := tmpl.Front().Value.(map[string]string)["nickname"]
		m["nickname"] = nickname
		mArr[i] = m
		i++
	}
	if l.Len() > 0 {
		this.Data["blockinfo"] = l.Front().Value.(map[string]string)
	} else {
		this.Data["blockinfo"] = make(map[string]string)
	}
	this.Data["details"] = mArr
	this.TplName = "show.tpl"
}

func (this *GameController) All() {
	openid := this.GetString("openid")

	l, err := db.Dia.Query("select min(reward) as reward,height,blockhash,max(DATE_ADD(update_time,interval 8 HOUR)) as update_time ,max(DATE_ADD(create_time,interval 8 HOUR)) as create_time from elastos_lottery_details where  blockhash is not null group by height , blockhash  having create_time > STR_TO_DATE('2018-08-15','%Y-%m-%d') order by height desc")
	if err != nil {
		beego.Error(err)
		this.Data["msg"] = "internal error"
		this.TplName = "error.html"
		return
	}
	mArr := make([]map[string]string, 0)
	var i int
	for e := l.Front(); e != nil; e = e.Next() {
		m := e.Value.(map[string]string)
		if err != nil {
			beego.Error(err)
		}
		mArr = append(mArr, m)
		i++
	}
	this.Data["all"] = mArr
	this.Data["openid"] = openid
	this.TplName = "all.tpl"
}

func getHashByHeight(height string) string {
	req := httplib.Get(beego.AppConfig.String("GetBlockByHeight") + "/" + height)
	respStr, err := req.String()
	if err != nil {
		beego.Error(err)
		return ""
	}
	var retMap map[string]interface{}
	err = json.Unmarshal([]byte(respStr), &retMap)
	if retMap["Error"].(float64) == 0 {
		return retMap["Result"].(map[string]interface{})["hash"].(string)
	}
	return ""
}
