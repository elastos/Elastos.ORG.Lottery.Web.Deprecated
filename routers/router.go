package routers

import (
	"Elastos.ORG.Lottery.Web/controllers"
	"github.com/astaxie/beego"
)

func init() {
	beego.Router("/index", &controllers.IndexController{})
	beego.Router("/submitAddr/:addr", &controllers.IndexController{}, "get:SubmitAddr")
	beego.Router("/home/:openid", &controllers.HomeController{})
	beego.Router("/registerUser/:openid", &controllers.IndexController{}, "get:RegisterUser")
	beego.Router("/game_home/", &controllers.GameController{})
	beego.Router("/submit_lottery", &controllers.GameController{}, "get:SubmitAnswer")
	beego.Router("/lotterydetail/:height", &controllers.GameController{}, "get:Detail")
	beego.Router("/lotteryall", &controllers.GameController{}, "get:All")
	beego.Router("/board", &controllers.BoardController{})
	beego.Router("/isGuessed", &controllers.GameController{}, "get:IsGuessed")
}
